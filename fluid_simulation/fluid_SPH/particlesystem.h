#include "water.h"

// Forward declaration of a pointer
class Water;
class ParticleSystem{
private:
	float h;
	float hxh;
	float gamma;
	float K;
	float EK;
	float sigma;
	float rho;
	float zeta;
	float s_threshold;
	float s_co;
	float viscosity_co;
	float max_velocity;
	float max_velocity_sqrt;
	float max_acceleration;
	float max_acceleration_sqrt;
	float damp;
	float kernel_poly6;
	float kernel_poly6_diff;
	float kernel_poly6_2diff;
	float kernel_spiky;
	float kernel_spiky_diff;
	float kernel_laplacian;
	float PI = 3.1415926f;
	glm::vec3 centre;
public:
	Water* water;
	vector<vector<Particle*>> partition;
	struct Wall
	{
		glm::vec3 wall;
		glm::vec3 normal;
	};
	struct neigh_cell
	{
		int index;
		float dist;
	};
	ParticleSystem();
	virtual int InitParticles(glm::vec3 _centre);
	virtual void Draw();
	virtual void Intergrate(float deltaTime);
	virtual void AddForce(const glm::vec3 direction);
	virtual void CollisionHandler(Particle *p1, Particle *p2);
	virtual void CollisionHandler(vector<Particle*> * cell_1, vector<Particle*> * cell_2);
	virtual void CollisionDetection(float deltaTime);
	virtual void Sorting();
	virtual void Update();
	virtual void Isosurface(float isolevel, float size);
};