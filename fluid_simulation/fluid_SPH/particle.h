#pragma once
#include <graphics_framework.h>
#include <iostream>

using namespace std;
using namespace graphics_framework;

class Particle
{
private:
	float mass;
	float dampValue;
	float viscosity;
	float density;
	float pressure;
	glm::vec3 force;
	glm::vec3 pos;
	glm::vec3 oldPos;
	glm::vec3 acceleration;
	glm::vec3 velocity;
public:
	Particle(const glm::vec3& pos, float _mass) : pos(pos), oldPos(pos), acceleration(glm::vec3(0.0f, 0.0f, 0.0f)), velocity(glm::vec3(0.0f, 0.0f, 0.0f)), density(1000.0f)
		, pressure(0.0f),force(glm::vec3(0.0f, 0.0f, 0.0f)), mass(_mass), dampValue(0.9f){}
	Particle(){}

	void addForce(glm::vec3 f)
	{
		acceleration += f / mass;
		//cout<<"xx"<<accerlation.y
	}

	void addAcceleration(glm::vec3 acc)
	{
		acceleration += acc;
	}

	void integrate(float deltaTime)
	{
		velocity += acceleration * deltaTime * dampValue;
		pos += velocity * deltaTime;
		//DBG_ASSERT(position.x >= 0.0f);
		acceleration = glm::vec3(0, 0, 0);
		force = glm::vec3(0, 0, 0);
	}
	void setDensity(float dens) { density = dens; }
	void setPressure(float p) { pressure = p; }
	void setpos(const glm::vec3& v) { pos = v; }
	void setVelocity(const glm::vec3& v) { velocity = v; }
	float getMass() { return mass; }
	float getPressure() { return pressure; }
	float getDensity() { return density; }
	const glm::vec3& getPos() const { return pos; }
	const glm::vec3& getVelocity() const { return velocity; }
	void offsetPos(const glm::vec3& v) { pos += v; }
};