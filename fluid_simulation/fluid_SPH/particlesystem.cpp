/*
Haoyang Hou.

*/

#include "particlesystem.h"
#include "marching_cubes.h"
struct isopoint
{
	glm::vec3 position;
	float value;
};

float radius = 0.1f;
vector<ParticleSystem::Wall> cubesides;
vector<Cube> cubegrid;
vector<isopoint> isopoints;
vector<glm::vec3> iso;
vector<vector<ParticleSystem::neigh_cell>> neighbour;
int water_num = 1200;
float cubesize = 4.0f;
float cubelength = 4.0f;
float lod = 0.1f;
float isosize = cubesize / lod;
float hook_wall = 80.5f;
float damp = 0.01f;
float hook_ball = 100.5f;
int gridsize = cubelength / (radius * 2);
float mass = 3.1415926f*radius * radius * radius * 1000.0f*1.333;
//int cells = gridsize * gridsize * gridsize;
int totalcells = gridsize * gridsize * gridsize;
//int totalcells = (gridsize + 1) * (gridsize + 1) * (gridsize + 1);
//timer_chrono timer1(30,"optimized_"+to_string(water_num),"o_"+to_string(water_num),false);
ParticleSystem::ParticleSystem()
{
	//cloth = new Cloth(2, 4, 20, 80);
	water = new Water(water_num, glm::vec3(0.0f), glm::vec3(cubesize));
	h = radius * 5;
	hxh = h*h;
	gamma = 1.0f;
	K = 314.0f;
	rho = 400.0f;
	sigma = 0.0f;
	zeta = -1.0f;
	viscosity_co = 0.008f;
	s_threshold = 2.0f;
	s_co = 1.0f;
	max_velocity = 500.0f;
	max_velocity_sqrt = sqrtf(max_velocity);
	max_acceleration = 500.0f;
	max_acceleration_sqrt = sqrtf(max_acceleration);
	damp = 0.6f;
	kernel_poly6 = 315.0f / (64.0f*PI*powf(h, 9.0f));
	kernel_poly6_diff = -945.0f / (32.0f*PI*powf(h, 9.0f));
	kernel_poly6_2diff = 945.0f / (8.0f*PI*powf(h, 9.0f));
	kernel_spiky = 15.0f / (PI*powf(h, 6.0f));
	kernel_spiky_diff = -45.0f / (PI*powf(h, 6.0f));
	kernel_laplacian = 45.0f / (PI*powf(h, 6.0f));
}

int ParticleSystem::InitParticles(glm::vec3 _centre)
{
	centre = _centre;
	water->initialise(mass);
	//cloth->Initialise();
	Wall yplus;
	yplus.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	yplus.wall = glm::vec3(0.0f);
	cubesides.push_back(yplus);

	Wall yminus;
	yminus.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	yminus.wall = glm::vec3(0.0f, cubesize, 0.0f);
	cubesides.push_back(yminus);

	Wall xplus;
	xplus.normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	xplus.wall = glm::vec3(0.0f);
	cubesides.push_back(xplus);

	Wall xminus;
	xminus.normal = glm::vec3(1.0f, 0.0f, 0.0f);
	xminus.wall = glm::vec3(cubesize, 0.0f, 0.0f);
	cubesides.push_back(xminus);

	Wall zplus;
	zplus.normal = glm::vec3(0.0f, 0.0f, -1.0f);
	zplus.wall = glm::vec3(0.0f, 0.0f, 0.0f);
	cubesides.push_back(zplus);

	Wall zminus;
	zminus.normal = glm::vec3(0.0f, 0.0f, 1.0f);
	zminus.wall = glm::vec3(0.0f, 0.0f, cubesize);
	cubesides.push_back(zminus);
	neighbour.resize(water_num);

	//isopoints.resize(pow(isosize, 3));

	//for (int x = 0; x < isosize; ++x)
	//{
	//	for (int y = 0; y < isosize; ++y)
	//	{
	//		for (int z = 0; z < isosize; ++z)
	//		{
	//			isopoints[x*isosize*isosize + y*isosize + z].position = glm::vec3(x * lod, y * lod, z * lod);
	//			isopoints[x*isosize*isosize + y*isosize + z].value = 0.0f;
	//		}
	//	}
	//}

	//for (int x = 0; x < isosize-1; ++x)
	//{
	//	for (int y = 0; y < isosize-1; ++y)
	//	{
	//		for (int z = 0; z < isosize-1; ++z)
	//		{
	//			Cube cube;
	//			cube.index[0] = (x * isosize *isosize+ y * isosize+ z);			
	//			cube.index[1] = ((x + 1) * isosize * isosize + y * isosize + z);
	//			cube.index[2] = ((x + 1) * isosize *isosize + y * isosize + (z + 1));
	//			cube.index[3] = (x * isosize * isosize + y * isosize + (z + 1));
	//			cube.index[4] = (x * isosize * isosize + (y+1) * isosize + z);
	//			cube.index[5] = ((x + 1) * isosize*isosize + (y + 1) * isosize + z);
	//			cube.index[6] = ((x + 1) * isosize *isosize + (y + 1) * isosize + (z + 1));
	//			cube.index[7] = (x * isosize *isosize + (y + 1) * isosize + (z + 1));

	//			for (int i = 0; i < 8; ++i)
	//			{
	//				cube.p[i] = isopoints[cube.index[i]].position;
	//				cube.val[i] = 0.0f;
	//			}

	//			cubegrid.push_back(cube);
	//		}
	//	}
	//}



	//partition.resize(u_cells);
	//partition.reserve(gridsize*gridsize*gridsize);
	partition.resize(totalcells);
	
	return 0;
}

void ParticleSystem::Draw()
{
	water->draw(radius);
	//Isosurface(radius, isosize);
	//glBegin(GL_TRIANGLES);
	//glColor3f(0.0f, 0.3f, 0.8f);
	//for (unsigned int i = 0; i < iso.size()-2; i+=3)
	//{
	//	glVertex3f(iso[i].x, iso[i].y, iso[i].z);
	//	glVertex3f(iso[i+1].x, iso[i+1].y, iso[i+1].z);
	//	glVertex3f(iso[i+2].x, iso[i+2].y, iso[i+2].z);
	//}
	//glEnd();
	//iso.clear();


	/*glBegin(GL_POINTS);
	for (int i = 0; i < isopoints.size(); ++i)
	{
		for (int j = 0; j < 8; ++j)
		{
			glVertex3f(isopoints[i].position.x, isopoints[i].position.y, isopoints[i].position.z);
		}
	}
	glEnd();*/
}

void ParticleSystem::AddForce(const glm::vec3 direction)
{
	water->addForce(direction);
	//cloth->AddForce(direction*0.1);
	//cloth->WindForce(glm::vec3(0.5f, 0.1f, 0.0f));
}

void ParticleSystem::Intergrate(float deltaTime)
{
	water->integrate(deltaTime);
	//cloth->Integrate(deltaTime);
}

void ParticleSystem::Update()
{
	Sorting();
	//Update Pressure
	for (int i = 0; i < water->particles.size(); ++i)
	{
		float d = 0.0f;
		for (int j = 0; j < neighbour[i].size(); ++j)
		{
			float c = hxh - neighbour[i][j].dist * neighbour[i][j].dist;
			d += mass * kernel_poly6 * c*c*c;
		}
		d = max(d, rho);
		water->particles[i].setDensity(d);
		float p = K*(d - rho);
		water->particles[i].setPressure(p);
	}

	//Update Acceleration
	for (int i = 0; i < water->particles.size(); ++i)
	{
		glm::vec3 force = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 pos = water->particles[i].getPos();
		glm::vec3 s_normal = glm::vec3(0.0f, 0.0f, 0.0f);
		float sf = 0.0f;
		for (int j = 0; j < neighbour[i].size(); ++j)
		{
			float nb_index = neighbour[i][j].index;
			float dist = neighbour[i][j].dist;
			float dxd = dist * dist;
			//Force from pressure
			float c1 = -0.5f*mass*(water->particles[i].getPressure() + water->particles[nb_index].getPressure());
			float c2 = kernel_spiky_diff*(h - dist)*(h - dist);
			float c3 = 1.0f / water->particles[nb_index].getDensity();

			force += glm::normalize(water->particles[i].getPos() - water->particles[nb_index].getPos())* c1 *c2 * c3;
			////Viscosity
			//float c4 = viscosity_co * kernel_laplacian * (h - dist) * mass / water->particles[nb_index].getDensity();
			//force += (water->particles[nb_index].getVelocity() - water->particles[i].getVelocity()) * c4;

			////Surface tension
			//float s1 = mass / water->particles[nb_index].getDensity();
			//s_normal += (water->particles[i].getPos() - water->particles[nb_index].getPos())*kernel_poly6_diff * (hxh - dxd)*(hxh - dxd)*s1;
			//sf += kernel_poly6_2diff * (hxh - dxd)*(1.75f*dxd - 0.75f*hxh)*s1;
		}
			float s_length = glm::length(s_normal);
			if (s_length > s_threshold){
				force += -glm::normalize(s_normal) * sf * s_co;
			}
		water->particles[i].addForce(force);
		water->particles[i].addAcceleration(glm::vec3(0.0f, -9.8f, 0.0f));
		neighbour[i].clear();
	}


}

void ParticleSystem::Sorting()
{
	for (int i = 0; i < totalcells; ++i)
	{
		partition[i].clear();
	}

	for (int i = 0; i < water->particles.size(); i++)
	{
		int x = 0, y = 0, z = 0;
		glm::vec3 pos = water->particles[i].getPos();
		if (pos.x < 0 || pos.x > cubesize || pos.y > cubesize || pos.y < 0 || pos.z > cubesize || pos.z < 0) continue;

		x = (int)((pos.x ) / (radius * 2));
		y = (int)((pos.y ) / (radius * 2));  
		z = (int)((pos.z ) / (radius * 2));

		int total = x * gridsize * gridsize + y * gridsize + z;
		partition[total].push_back(&water->particles[i]);

		

		/*x = (int)((pos.x) / lod);
		y = (int)((pos.y) / lod);
		z = (int)((pos.z) / lod);

		for (int i = -(radius / lod); i < radius / lod; ++i)
		{
			for (int j = -(radius / lod); j < radius / lod; ++j)
			{
				for (int q = -(radius / lod); q < radius / lod; ++q)
				{
					if ((x+i) < isosize && x+i > 0 && y+j > 0 && (y+j) < isosize && z+q > 0 && (z+q) < isosize)
					{
						int index = (x+i) * isosize * isosize + (y+j) * isosize + (z+q);
						if (glm::distance(isopoints[index].position, pos) < radius)
							isopoints[index].value += 1.0f;
					}
				}
			}
		}*/
	
	}
	for (int i = 0; i < water->particles.size(); i++)
	{
		for (int j = i + 1; j < water->particles.size() - 1; j++)
		{
			float distance = glm::length(water->particles[i].getPos() - water->particles[j].getPos());
			if (distance < h)
			{
				ParticleSystem::neigh_cell a;
				a.dist = distance;
				a.index = j;
				neighbour[i].push_back(a);
				a.index = i;
				neighbour[j].push_back(a);
			}
		}
	}
}

void ParticleSystem::Isosurface(float isolevel, float size)
{
	for (int i = 0; i < cubegrid.size(); ++i)
	{
		//Iterate all particles in this cell
	
		for (int m = 0; m < 8; ++m)
		{
			cubegrid[i].val[m] = isopoints[cubegrid[i].index[m]].value;
			//assert(cubegrid[i].val[m] > 0.0f);
		}
		//Polygonise the cube and store traingles in the iso
		int result = Polygonise(cubegrid[i], isolevel, iso);
		//assert(iso.size() > 0);
		//Clear the cube value for next use.
		for (int m = 0; m < 8; ++m)
		{
			cubegrid[i].val[m] = 0.0f;
		}
	}

	for (int i = 0; i < isopoints.size(); ++i)
	{
		isopoints[i].value = 0;
	}

}

void ParticleSystem::CollisionDetection(float deltaTime)
{
	std::vector<Particle>::iterator particle;

	for (particle = water->particles.begin(); particle != water->particles.end(); particle++)
	{
		//get the velocity of this particle.
		glm::vec3 velocity = (*particle).getVelocity();
		glm::vec3 pos = (*particle).getPos();
		for (auto i : cubesides)
		{
			float penalty = radius - glm::dot(i.wall - pos, i.normal);
			if (penalty <= 0) continue;
			particle->addAcceleration(-i.normal*penalty*hook_wall);
			particle->addAcceleration(-velocity * 2.0f);
		}
		//cloth->BallCollision(pos, radius);
	}

}

void ParticleSystem::CollisionHandler(Particle *p1, Particle *p2)
{
	if (p1 == NULL || p2 == NULL) return;
	glm::vec3 dis = (*p1).getPos() - (*p2).getPos();		//get the current distance.
	float length = glm::length(dis);							//get length of distance.
	glm::vec3 normal = glm::normalize(dis);

	float penalty = 2 * radius - length;
	if (penalty > 0)
	{
		(*p1).addForce(normal*penalty*hook_ball);
		(*p2).addForce(-normal*penalty*hook_ball);
	}

}

void ParticleSystem::CollisionHandler(vector<Particle*> *cell_1, vector<Particle*> *cell_2)
{
	if (cell_1->size() != 0 && cell_2->size() != 0)
	{
		for (int i = 0; i < cell_1->size(); ++i)
		{
			for (int j = 0; j < cell_2->size(); ++j)
				CollisionHandler((*cell_1)[i], (*cell_2)[j]);
		}
	}
}

