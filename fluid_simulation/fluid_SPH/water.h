#pragma once
#include "particle.h"
#include <vector>

class Water
{
private:
	int num_particles;
	glm::vec3 minrange;
	glm::vec3 maxrange;
public:
	Water(int num, glm::vec3 _min, glm::vec3 _max);
	Water();
	vector<mesh> water_mesh;
	std::vector<Particle> particles;
	std::vector<Particle>::iterator particle;
	virtual void initialise(float mass);
	virtual void draw(float radius);
	virtual void integrate(float deltaTime);
	virtual void addForce(const glm::vec3 direction);
};