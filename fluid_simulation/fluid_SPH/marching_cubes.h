#pragma once

#include <vector>
#include <graphics_framework.h>

using namespace std;

struct Tri
{
	glm::vec3 p[3];
};

struct Cube
{
	glm::vec3 p[8];
	float val[8];
	int index[8];
};

/*
Given a grid cell and an isolevel, calculate the triangular
facets required to represent the isosurface through the cell.
Return the number of triangular facets, the array "triangles"
will be loaded up with the vertices at most 5 triangular facets.
0 will be returned if the grid cell is either totally above
of totally below the isolevel.
*/
int Polygonise(const Cube& grid, float isolevel, vector<glm::vec3>& triangles);

/*
Linearly interpolate the position where an isosurface cuts
an edge between two vertices, each with their own scalar value
*/
glm::vec3 VertexInterp(float isolevel, const glm::vec3& p1, const glm::vec3& p2, float valp1, float valp2);
