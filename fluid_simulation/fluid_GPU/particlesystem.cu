#include "particlesystem.cuh"

__constant__ __device__ unsigned particle_num;
__constant__ SPHparameter sphparam;
__constant__ __device__ float mass;
void initsph(particlesystem& fluid,unsigned int number,grid& Grid)
{
	//Allocate memory
	fluid.particle_num = number;
	cudaMemcpyToSymbol(particle_num, &number, sizeof(unsigned));
	fluid.mass = 0.1f;
	cudaMemcpyToSymbol(mass, &fluid.mass, sizeof(float));
	fluid.radius = 0.1f;
	fluid.pos = new float3[number];
	fluid.acc = new float3[number];
	fluid.vel = new float3[number];
	fluid.density = new float[number];
	fluid.pressure = new float[number];
	fluid.cellindex = new unsigned[number];
	fluid.pID = new unsigned[number];
	fluid.wall = new bounding[6];
	fluid.gravity = make_float3(0.0f, -9.8f, 0.0f);
	//fluid.gravity = make_float3(0.0f, 0.0f, 0.0f);
	//Initialise the particle position 
	int size = pow(number, 1.0f/3)+1;
	unsigned index = 0;
	for (int x = 0; x < size; ++x)
	{
		for (int y = 0; y < size; ++y)
		{
			for (int z = 0; z < size; ++z)
			{
				if (index >= number)
				{
					z = size + 1;
					y = size + 1;
					x = size + 1;
					break;
				}
				fluid.pos[index] = make_float3(fluid.radius + x*2*fluid.radius, fluid.radius + y*2*fluid.radius, fluid.radius+z*2* fluid.radius);
				index += 1;
			}
		}
	}
	/*for (unsigned i = 0; i < number; ++i)
	{
		fluid.pos[i] = make_float3(4.0f-i*0.2f, 4.0f-i*0.2f, 4.0f-i*0.2f);
	}*/
	for (unsigned i = 0; i < number; ++i)
	{
		fluid.density[i] = 0.0f;
		fluid.acc[i] = make_float3(0.0f, 0.0f, 0.0f);
		fluid.pressure[i] = 0.0f;
		fluid.vel[i] = make_float3(0.0f, 0.0f, 0.0f);
	}
	//Initialise general environment
	fluid.ambient_density = 1.0f;
	fluid.container_size = make_float3(10.0f,10.0f,10.0f);
	fluid.damp = 0.01f;
	fluid.hook_wall = 400.0f;
	fluid.cellsize = 0.5f;
	fluid.gridsize = make_uint3(fluid.container_size.x / fluid.cellsize,fluid.container_size.y/fluid.cellsize,fluid.container_size.z/fluid.cellsize);
	fluid.totalcells = fluid.gridsize.x* fluid.gridsize.y*fluid.gridsize.z;
	bounding wall;
	//y plus wall
	wall.normal = make_float3(0.0f, -1.0f, 0.0f);
	wall.position = make_float3(0.0f,0.0f,0.0f);
	fluid.wall[0] = wall;
	//y minus wall
	wall.normal = make_float3(0.0f, 1.0f, 0.0f);
	wall.position = make_float3(0.0f, fluid.container_size.y, 0.0f);
	fluid.wall[1] = wall;
	//X plus wall
	wall.normal = make_float3(-1.0f, 0.0f, 0.0f);
	wall.position = make_float3(0.0f,0.0f, 0.0f);
	fluid.wall[2] = wall;
	//X minus wall
	wall.normal = make_float3(1.0f, 0.0f, 0.0f);
	wall.position = make_float3(fluid.container_size.x,0.0f, 0.0f);
	fluid.wall[3] = wall;
	//Z plus wall
	wall.normal = make_float3(0.0f, 0.0f, -1.0f);
	wall.position = make_float3(0.0f, 0.0f, 0.0f);
	fluid.wall[4] = wall;
	//Z minus wall
	wall.normal = make_float3(0.0f, 0.0f, 1.0f);
	wall.position = make_float3(0.0f, 0.0f,fluid.container_size.z);
	fluid.wall[5] = wall;

	//Pass wall data to GPU
	bounding* d_wall;
	HandleError(cudaMalloc(&d_wall, 6 * sizeof(bounding)));
	HandleError(cudaMemcpy(d_wall, fluid.wall, 6 * sizeof(bounding), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.wall;
	//Store the GPU pointer
	fluid.wall = d_wall;

	//Pass position data to GPU, store the address to d_pos and use it as GPU pointer
	HandleError(cudaMalloc(&fluid.d_pos, number*sizeof(float3)));
	HandleError(cudaMemcpy(fluid.d_pos,fluid.pos, number*sizeof(float3), cudaMemcpyHostToDevice));

	//Pass acceleration data
	float3* d_acc;
	HandleError(cudaMalloc(&d_acc, number*sizeof(float3)));
	HandleError(cudaMemcpy(d_acc, fluid.acc, number*sizeof(float3), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.acc;
	//Store the GPU pointer
	fluid.acc = d_acc;

	//Pass velocity data
	float3* d_vel;
	HandleError(cudaMalloc(&d_vel, number*sizeof(float3)));
	HandleError(cudaMemcpy(d_vel, fluid.vel, number*sizeof(float3), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.vel;
	//Store the GPU pointer
	fluid.vel = d_vel;

	//Pass density data
	float* d_density;
	HandleError(cudaMalloc(&d_density, number*sizeof(float)));
	HandleError(cudaMemcpy(d_density, fluid.density, number*sizeof(float), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.density;
	//Store the GPU pointer
	fluid.density = d_density;

	//Pass pressure data
	float* d_pressure;
	HandleError(cudaMalloc(&d_pressure, number*sizeof(float)));
	HandleError(cudaMemcpy(d_pressure, fluid.pressure, number*sizeof(float), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.pressure;
	//Store the GPU pointer
	fluid.pressure = d_pressure;

	//Pass cell index array
	unsigned* d_cellindex;
	HandleError(cudaMalloc(&d_cellindex, number*sizeof(unsigned)));
	HandleError(cudaMemcpy(d_cellindex, fluid.cellindex, number*sizeof(unsigned), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.cellindex;
	//Store the GPU pointer
	fluid.cellindex = d_cellindex;

	//Pass particle id key array
	unsigned* d_pID;
	HandleError(cudaMalloc(&d_pID, number*sizeof(unsigned)));
	HandleError(cudaMemcpy(d_pID, fluid.pID, number*sizeof(unsigned), cudaMemcpyHostToDevice));
	//Free the CPU memory
	delete[] fluid.pID;
	//Store the GPU pointer
	fluid.pID = d_pID;

	//Wrap raw GPU pointer for sorting
	fluid.t_cellindex = thrust::device_ptr<unsigned>(fluid.cellindex);
	fluid.t_pID = thrust::device_ptr<unsigned>(fluid.pID);
	//Initialise the grid
	initgrid(fluid.gridsize.x, fluid.gridsize.y, fluid.gridsize.z, Grid);
	computeGridSize(fluid.particle_num, 128, fluid.blockNum, fluid.threadNum);

	//SPh parameter
	SPHparameter normal;
	normal.h = 5 * fluid.radius;
	normal.hxh = normal.h * normal.h;
	normal.gamma = 1.0f;
	normal.K = 16.0f;
	normal.rho = 15.0f;
	normal.sigma = 0.0f;
	normal.zeta = -1.0f;
	normal.viscosity_c = 0.1f;
	normal.surface_c = 0.01f;
	normal.surface_t = 1.8f;
	normal.PI = 3.1415926f;
	normal.kernel_poly6 = 315.0f / (64.0f*normal.PI*powf(normal.h, 9.0f));
	normal.kernel_poly6_gradient = -945.0f / (32.0f*normal.PI*powf(normal.h, 9.0f));
	normal.kernel_poly6_laplacian = 945.0f / (8.0f*normal.PI*powf(normal.h, 9.0f));
	normal.kernel_spikey = 15.0f / (normal.PI*powf(normal.h, 6.0f));
	normal.kernel_spikey_gradient = -45.0f / (normal.PI*powf(normal.h, 6.0f));
	normal.kernel_spikey_laplacian = 45.0f / (normal.PI*powf(normal.h, 6.0f));
	HandleError(cudaMemcpyToSymbol(sphparam, &normal, sizeof(SPHparameter)));
	
}

void RunSPH(particlesystem& fluid, grid& Grid)
{
	Sorting(fluid, Grid);
	//test << <1, 10 >> >(fluid.acc, 0);
	CheckError();
	CheckError();
	//test1 << <10, 100 >> >(Grid.offsets, fluid.totalcells);
	//test2 << <1, 10 >> >(fluid.vel, 0);
	update_density << <fluid.blockNum, fluid.threadNum >> >(fluid.d_pos, fluid.acc, fluid.density, fluid.pressure, Grid.counts, Grid.offsets, fluid.cellindex, fluid.pID, Grid.neighbours,Grid.nbcounts);
	CheckError();
	update_pressure << <fluid.blockNum, fluid.threadNum >> >(fluid.d_pos, fluid.vel,fluid.acc, fluid.density, fluid.pressure, Grid.counts, Grid.offsets, fluid.cellindex, fluid.pID, Grid.neighbours, Grid.nbcounts);
	CheckError();
	update_external<<<fluid.blockNum,fluid.threadNum>>>(fluid.d_pos, fluid.vel, fluid.gravity, fluid.acc, fluid.wall,fluid.hook_wall,fluid.radius);
	//test << <1, 10 >> >(fluid.d_pos, 0);
	CheckError();
	//test2 << <1, 10 >> >(fluid.vel, 0);
	integrate << <fluid.blockNum, fluid.threadNum >> >(fluid.d_pos, fluid.acc, fluid.vel);
	//test2 << <1, 10 >> >(fluid.vel, 0)
	//test << <1, 10 >> >(fluid.d_pos, 0);
	CheckError();

	HandleError(cudaMemcpy(fluid.pos, fluid.d_pos, fluid.particle_num*sizeof(float3), cudaMemcpyDeviceToHost));
	//cudaDeviceSynchronize();
	//CheckError();
	//CheckError();
}

void Sorting(particlesystem& fluid,grid& Grid)
{
	clear_grid << <Grid.block, Grid.zdim >> >(Grid.counts, Grid.offsets);
	CheckError();
	CheckError();
	//checkcell << <1, 100 >> >(Grid.counts, Grid.offsets, Grid.xyzdim);
	CheckError();
	//Partition the particles and fill the cellindex array
	partition<<<fluid.blockNum,fluid.threadNum>>>(Grid.counts, fluid.d_pos, fluid.cellindex,fluid.pID,fluid.cellsize, fluid.gridsize);
	CheckError();
	//checkcell << <1, 100 >> >(Grid.counts, Grid.offsets, Grid.xyzdim);
	CheckError();
	CheckError();
	thrust::sort_by_key(fluid.t_cellindex, fluid.t_cellindex+fluid.particle_num, fluid.t_pID);
	//checkcell << <1, 100 >> >(Grid.counts, Grid.offsets, Grid.xyzdim);
	CheckError();
	CheckError();
	thrust::exclusive_scan(Grid.t_counts, Grid.t_counts + Grid.xyzdim, Grid.t_offsets);
	//checkcell << <1, 100 >> >(Grid.counts, Grid.offsets, Grid.xyzdim);
	//test1 << <1, 10 >> >(Grid.counts, 0);
	CheckError();
	CheckError();
}

void Stop(particlesystem& fluid, grid& Grid)
 {
	delete[] fluid.pos;
	 cudaFree(fluid.wall);
	 cudaFree(fluid.acc);
	 cudaFree(fluid.d_pos);
	 cudaFree(fluid.vel);
	 cudaFree(fluid.density);
	 cudaFree(fluid.pressure);
	 cudaFree(fluid.cellindex);
	 cudaFree(fluid.pID);
	 cudaFree(Grid.neighbours);
	 cudaFree(Grid.counts);
	 cudaFree(Grid.offsets);
	 
 }

 __global__ void test1(unsigned* cellindex, unsigned n)
 {
	/* unsigned index = n + threadIdx.x;
	 printf("The value of %d is %d\n", index, cellindex[index]);*/
	 unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	 if (index >= n) return;
	 printf("particle %d cell index%d\n", index, cellindex[index]);
 }

 __global__ void checkcell(unsigned* counts, unsigned* offsets, unsigned n)
 {
	 unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	 if (index >= n) return;
	 printf("index %d, counts %d, offsets %d\n", index, counts[index], offsets[index]);
 }

 __global__ void test(float3* d_pos, unsigned n)
 {
	 unsigned index = n + threadIdx.x;
	 printf("The pos of %d is %f,%f,%f\n", index,d_pos[index].x, d_pos[index].y,d_pos[index].z);
 }

 __global__ void test2(float3* vel, unsigned n)
 {
	 unsigned index = n + threadIdx.x;
	 printf("The vel of %d is %f,%f,%f\n", index, vel[index].x, vel[index].y, vel[index].z);
 }

__global__ void partition(unsigned* count, float3* pos, unsigned* cellindex,unsigned* pindex,float cellsize,uint3 gridsize)
{
	unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= particle_num) return;
	int x = pos[index].x / cellsize;
	if (x >= gridsize.x) 
		x = gridsize.x-1;
	if (x < 0) 
		x = 0;

	int y = pos[index].y / cellsize;
	if (y >= gridsize.y) 
		y = gridsize.y - 1;
	if (y < 0) 
		y = 0;
	int z = pos[index].z / cellsize;
	if (z >= gridsize.z) 
		z = gridsize.z - 1;
	if (z < 0) 
		z = 0;

	unsigned cell_index = dimtoindex(x, y, z, gridsize.x, gridsize.y, gridsize.z);
	pindex[index] = index;
	//Cellindex indicate which point is paired with which cell.
	cellindex[index] = cell_index;
	//printf("particle %d cell index%d\n",index, cell_index);
	atomicAdd(&(count[cell_index]),(unsigned)1);
}

__device__ int dimtoindex(int x, int y, int z, int xdim, int ydim, int zdim)
{
	int index = x*ydim*zdim + y * zdim + z;
	return index;
}



__global__ void update_density(float3* d_pos, float3* acc, float* density, float* pressure, unsigned* counts, unsigned* offsets, unsigned* cellindex, unsigned* pID, unsigned* neighbours,unsigned* nbcounts)
{
	unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= particle_num) return;
	//printf("index %d\n", index);
	//printf("cell index 0", cellindex[0]);
	//particle id
	unsigned pindex = pID[index];
	//printf("pindex %d", pindex);
	//Get the cell this particle belongs to.
	unsigned cell_index = cellindex[index];
	//printf("index %d cell index %d\n", index,cell_index);
	float3 pos = d_pos[pindex];
	float dens = 0.0f;
	//printf("index %d\n",index);
	for (unsigned i = 0; i < nbcounts[cell_index]; ++i)
	{
		unsigned cell = neighbours[cell_index*27 + i];
		//printf("index %d\n", index);
		unsigned start = offsets[cell];
	//	printf("index %d\n", index);
		unsigned end = counts[cell];
		//printf("index %d\n", index);
		for (unsigned j = start; j < start+end; ++j)
		{
			unsigned nbpID = pID[j];
			float dist = length(pos - d_pos[pID[j]]);
			if (dist <= sphparam.h && nbpID != pindex)
			{
				float c = sphparam.hxh - dist*dist;
				dens += mass*sphparam.kernel_poly6*c*c*c;
			}
		}
		//printf("index %d one step\n", index);
	}
	dens = max(dens, sphparam.rho);
	density[pindex] = dens;
	pressure[pindex] = sphparam.K * (dens- sphparam.rho);
	//printf("index\n");
}

__global__ void update_pressure(float3* d_pos,float3* vel, float3* acc, float* density, float* pressure, unsigned* counts, unsigned* offsets, unsigned* cellindex, unsigned* pID, unsigned* neighbours,unsigned* nbcounts)
{
	unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= particle_num) return;
	//particle id
	unsigned pindex = pID[index];
	//Get the cell this particle belongs to.
	unsigned cell_index = cellindex[index];
	//Get particle information
	float3 pos = d_pos[pindex];
	float pres = pressure[pindex];
	float3 accnow = make_float3(0.0f, 0.0f, 0.0f);
	float3 s_normal = make_float3(0.0f, 0.0f, 0.0f);
	float sf = 0.0f;
	for (unsigned i = 0; i < nbcounts[cell_index]; ++i)
	{
		unsigned cell = neighbours[cell_index*27 + i];
		for (unsigned j = offsets[cell]; j < offsets[cell]+counts[cell]; ++j)
		{
			unsigned nbpID = pID[j];
			float dist = length(pos - d_pos[nbpID]);
			if (dist <= sphparam.h && nbpID != pindex)
			{
				if (dist == 0) printf("oh"); 
				//Pressure
				accnow += -1.0f *((pressure[nbpID] + pres)/(2 * density[nbpID]))*sphparam.kernel_spikey_gradient*powf((sphparam.h - dist), 2.0f)*normalize(pos - d_pos[nbpID]);
				//Viscosity
				accnow += sphparam.viscosity_c*(vel[nbpID] - vel[pindex])*sphparam.kernel_spikey_laplacian*(sphparam.h - dist) / density[nbpID];
				//Surface Tension
				s_normal += (pos - d_pos[nbpID])*sphparam.kernel_poly6_gradient*powf((sphparam.hxh - dist*dist),2.0f)*mass / density[nbpID];
				sf += sphparam.kernel_poly6_laplacian*(sphparam.hxh - dist*dist)*(1.75f*dist*dist - 0.75f*sphparam.hxh) / density[nbpID];
			}
		}
	}
	if (length(s_normal) > sphparam.surface_t)
	{
		accnow += -sf*sphparam.surface_c*normalize(s_normal);
	}
	acc[pindex] = accnow;
}

__global__ void update_external(float3* pos, float3* vel, float3 gravity, float3* acc,bounding* bbox,float hookwall,float radius)
{

	unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= particle_num) return;
	for (unsigned i = 0; i < 6; ++i)
	{
		float penalty = radius - dot(bbox[i].position - pos[index], bbox[i].normal);
		if (penalty > 0)
		{
			acc[index] += -bbox[i].normal*penalty*hookwall;
			acc[index] += -vel[index] * 3.0f;
		}
	}
	acc[index] += gravity;
}

__global__ void integrate(float3* pos,float3* acc,float3* vel)
{
	unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
	if (index >= particle_num) return;
	vel[index] += acc[index] * 0.01;
	pos[index] += vel[index] * 0.01;
	acc[index] = make_float3(0.0f, 0.0f, 0.0f);
}

