#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <glm\gtx\component_wise.hpp>
#include "particlesystem.cuh"

#include <cuda_runtime_api.h>
#include <cuda_gl_interop.h>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
geometry box;
double cursor_x = 0.0;
double cursor_y = 0.0;
texture tex;
texture tex_iso;
texture tex_ball;
effect eff;
free_camera cam;
particlesystem psystem;
grid Grid;
float deltaTime = 0.001f;
unsigned int number = 10000;

bool initialise()
{
	// ********************************
	// Set input mode - hide the cursor
	// ********************************
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);
	return true;
}

bool load_content()
{
	//Create container mesh
	//meshes["box"] = mesh(geometry_builder::create_box());
	box = geometry_builder::create_sphere(10, 10, glm::vec3(0.1f));
	// Create plane mesh
	meshes["plane"] = mesh(geometry_builder::create_plane());
	
	// Load texture
	tex = texture("..\\resources\\textures\\floor3.jpg");
	tex_iso = texture("..\\resources\\textures\\red.png");
	tex_ball = texture("..\\resources\\textures\\water.jpg");
	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
	// Build effect
	eff.build();

	/*if (!initsystem(psystem))
		return false;*/
	//cout << psystem.pos[2].x << endl;
	initsph(psystem,number,Grid);
	// Set camera properties
	cam.set_position(vec3(0.0f, 3.0f, 15.0f));
	cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 0.414f, 1000.0f);

	return true;
}

bool update(float delta_time)
{
	// The ratio of pixels to rotation - remember the fov
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);

	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// ****************************************************************************
	// Multiply deltas by ratios and delta_time - gets actual change in orientation
	// ****************************************************************************
	delta_x *= ratio_width;
	delta_y *= ratio_height;

	// *************************
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	// *************************
	cam.rotate(delta_x, -delta_y);

	// ************************************
	// Use keyboard to move the target_mesh
	// Also remember to translate camera
	// - WSAD
	// ************************************
	// *******************************
	// Use keyboard to move the camera
	// - WSAD
	// *******************************
	vec3 translation(0.0f, 0.0f, 0.0f);
	if (glfwGetKey(renderer::get_window(), 'W'))
		translation.z += 10.0f * delta_time;
	if (glfwGetKey(renderer::get_window(), 'S'))
		translation.z -= 10.0f * delta_time;
	if (glfwGetKey(renderer::get_window(), 'A'))
		translation.x -= 10.0f * delta_time;
	if (glfwGetKey(renderer::get_window(), 'D'))
		translation.x += 10.0f * delta_time;




	// ***********
	// Move camera
	// ***********
	cam.move(translation);
	// *****************
	// Update the camera
	// *****************
	cam.update(delta_time);

	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;
	//psystem.Update();
	//psystem.CollisionDetection(deltaTime);
	//if (glfwGetKey(renderer::get_window(), 'J'))
	//	psystem.AddForce(glm::vec3(-500.0f, 0.0f, 0.0f));
	//if (glfwGetKey(renderer::get_window(), 'K'))
	//	psystem.AddForce(glm::vec3(0.0f, 0.0f, 500.0f));
	//if (glfwGetKey(renderer::get_window(), 'L'))
	//	psystem.AddForce(glm::vec3(500.0f, 0.0f, 0.0f));
	//if (glfwGetKey(renderer::get_window(), 'I'))
	//	psystem.AddForce(glm::vec3(0.0f, 0.0f, -500.0f));
	////psystem.AddForce(glm::vec3(0.0f, -9.8f, 0.0f));
	//psystem.Intergrate(deltaTime);
	RunSPH(psystem, Grid);
	return true;
}

bool isorender()
{
	renderer::bind(tex_ball, 0);
	glUniform1i(eff.get_uniform_location("tex"), 0);
	//psystem.Draw();
	return true;
}

bool ballrender()
{
	renderer::bind(eff);
	auto V = cam.get_view();
	auto P = cam.get_projection();
	for (unsigned int i = 0; i < number; ++i)
	{
		auto M = glm::translate(glm::mat4(1.0f), glm::vec3(
			psystem.pos[i].x, 
			psystem.pos[i].y, 
			psystem.pos[i].z));
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data
		// Bind and set texture
		renderer::bind(tex_ball, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);
		renderer::render(box);
	}

	return true;
}

bool render()
{
	ballrender();

	for (auto &e : meshes)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(eff);
		// Create MVP matrix
		//auto M = m.get_transform().get_transform_matrix();
		auto M = glm::mat4(1.0f);
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind and set texture
		renderer::bind(tex, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);
		// Render mesh
		renderer::render(m);
		//renderer::render(box);
	}
	//renderer::render(box);
	//isorender();
	return true;
}

void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_initialise(initialise);
	application.set_update(update);
	application.set_render(render);
	// Run application
	application.run();
	Stop(psystem, Grid);
}