#pragma once
#include "grid.cuh"
#include "errorcheck.h"


struct bounding
{
	float3 position;
	float3 normal; 
};

struct SPHparameter
{
	float h;
	float hxh;
	float gamma;
	float K;
	float EK;
	float sigma;
	float rho;
	float zeta;
	float surface_t;
	float surface_c;
	float viscosity_c;
	float PI;
	float kernel_poly6;
	float kernel_poly6_gradient;
	float kernel_poly6_laplacian;
	float kernel_spikey;
	float kernel_spikey_gradient;
	float kernel_spikey_laplacian;
};

struct particlesystem
{
	//The general environment 
	unsigned particle_num;
	float mass;
	float radius;
	float3 container_size;
	float hook_wall;
	float damp;
	float ambient_density;
	float cellsize;
	float3 gravity;
	uint3 gridsize;
	unsigned totalcells;
	bounding* wall;
	//Below is particle arrays
	float3* pos;
	float3* d_pos;
	float3* acc;
	float3* vel;
	float* density;
	float* pressure;
	//Cellindex and PID is a table. PID is key, cellindex is value.
	//Cellindex indicate for particle i, the cell it belongs to is cellindex[i]
	unsigned* cellindex;
	//In order to track the particle id, pID represent the ID matched in the cellindex array.
	unsigned* pID;
	//Thrust require safty pointer to be used in their algorithm.
	//Wrap cellindex pointer
	thrust::device_ptr<unsigned> t_cellindex;
	//Wrap pID pointer
	thrust::device_ptr<unsigned> t_pID;
	//Block num and threads num for launching kernels
	unsigned blockNum, threadNum;
	////Just for testing
	//unsigned* test;
	//unsigned* key;

};

void initsph(particlesystem& fluid,unsigned int number,grid& Grid);
void Sorting(particlesystem& fluid, grid& Grid);
void RunSPH(particlesystem& fluid, grid& Grid);
void Stop(particlesystem& fluid, grid& Grid);
__global__ void test(float3* pos, unsigned n);
__global__ void test2(float3* vel, unsigned n);
__global__ void test1(unsigned* cellindex, unsigned n);
__global__ void checkcounts(unsigned* counts, unsigned n);
__global__ void checkoffsets(unsigned* offsets, unsigned n);
__global__ void checkcell(unsigned* counts, unsigned* offsets, unsigned n);
__global__ void partition(unsigned* count, float3* pos, unsigned* cellindex,unsigned* pindex,float cellsize, uint3 gridsize);
__global__ void update_density(float3* d_pos, float3* acc,float* density, float* pressure, unsigned* counts, unsigned* offsets, unsigned* cellindex, unsigned* pID, unsigned* neighbours,unsigned* nbcounts);
__global__ void update_pressure(float3* d_pos, float3* vel,float3* acc, float* density, float* pressure, unsigned* counts, unsigned* offsets, unsigned* cellindex, unsigned* pID, unsigned* neighbours, unsigned* nbcounts);
__global__ void update_viscosity(particlesystem& fluid, grid& Grid);
__global__ void update_surfacetension(particlesystem& fluid, grid& Grid);
__global__ void update_external(float3* pos, float3* vel, float3 gravity, float3* acc, bounding* bbox,float hookwall,float radius);
__global__ void integrate(float3* pos,float3* acc,float3* vel);
__device__ int dimtoindex(int x, int y, int z, int xdim, int ydim, int zdim);
//calculate block number and thread number
inline __host__ __device__ void computeGridSize(unsigned n, unsigned blockSize, unsigned& numBlocks, unsigned& numThreads)
{
	if (blockSize < n)
		numThreads = blockSize;
	else
		numThreads = n;
	numBlocks = (n % numThreads != 0) ? (n / numThreads + 1) : (n / numThreads);
}
//plus
inline __host__ __device__ float3 operator+(float3 a, float3 b)
{
	return make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}
//plus equal
inline __host__ __device__ void operator+=(float3 &a, float3 b)
{
	a.x += b.x; a.y += b.y; a.z += b.z;
}
//minus
inline __host__ __device__ float3 operator-(float3 a, float3 b)
{
	return make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}
//minus equal
inline __host__ __device__ void operator-=(float3 &a, float3 b)
{
	a.x -= b.x; a.y -= b.y; a.z -= b.z;
}
// multiply
inline __host__ __device__ float3 operator*(float3 a, float3 b)
{
	return make_float3(a.x * b.x, a.y * b.y, a.z * b.z);
}
inline __host__ __device__ float3 operator*(float3 a, float s)
{
	return make_float3(a.x * s, a.y * s, a.z * s);
}
inline __host__ __device__ float3 operator*(float s, float3 a)
{
	return make_float3(a.x * s, a.y * s, a.z * s);
}
inline __host__ __device__ float3 operator/(float3 a, float s)
{
	return make_float3(a.x / s, a.y / s, a.z / s);
}
//dot
inline __host__ __device__ float dot(float3 a, float3 b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}
// cross product
inline __host__ __device__ float3 cross(float3 a, float3 b)
{
	return make_float3(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}
// length
inline __host__ __device__ float length(float3 v)
{
	return sqrtf(dot(v, v));
}
// normalize
inline __host__ __device__ float3 normalize(float3 v)
{
	float invLen = sqrtf(dot(v, v));
	if (invLen == 0) 
	return v * invLen;
	return v / invLen;
}
// max
//inline __host__ __device__ float max(float a, float b)
//{
//	if (a > b)
//		return a;
//	return b;
//}
// negate
inline __host__ __device__ float3 operator-(float3 &a)
{
	return make_float3(-a.x, -a.y, -a.z);
}