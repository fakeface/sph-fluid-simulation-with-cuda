#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <thrust\sort.h>
#include <thrust\device_ptr.h>
#include <stdio.h>
// Define this to turn on error checking
#define CUDA_ERROR_CHECK
#define HandleError( err ) _HandleError( err, __FILE__, __LINE__ )
#define CheckError()    _CheckError( __FILE__, __LINE__ )

inline void _HandleError(cudaError err, const char *file, const int line)
{
#ifdef CUDA_ERROR_CHECK
	if (cudaSuccess != err)
	{
		fprintf(stderr, "HandleError() failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
	}
#endif

	return;
}

inline void _CheckError(const char *file, const int line)
{
#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "CheckError() failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if (cudaSuccess != err)
	{
		fprintf(stderr, "CheckError() with sync failed at %s:%i : %s\n",
			file, line, cudaGetErrorString(err));
	}
#endif

	return;
}