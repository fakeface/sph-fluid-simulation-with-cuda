#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "errorcheck.h"
#include <stdio.h>

struct particle
{
	float3 pos;
	float3 acc;
	float3 vel;
	float density;
	float pressure;
	int cellindex;
};
