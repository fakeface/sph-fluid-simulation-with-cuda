#include "grid.cuh"


void initgrid(int x, int y, int z, grid& Grid)
{
	Grid.xdim = x;
	Grid.ydim = y;
	Grid.zdim = z;
	Grid.yzdim = y*z;
	Grid.xyzdim = x*y*z;
	HandleError(cudaMalloc(&Grid.neighbours, Grid.xyzdim*27*sizeof(unsigned)));
	HandleError(cudaMalloc(&Grid.nbcounts, Grid.xyzdim*sizeof(unsigned)));
	HandleError(cudaMalloc(&Grid.counts, Grid.xyzdim*sizeof(unsigned)));
	HandleError(cudaMalloc(&Grid.offsets, Grid.xyzdim*sizeof(unsigned)));
	Grid.t_counts = thrust::device_ptr<unsigned>(Grid.counts);
	Grid.t_offsets = thrust::device_ptr<unsigned>(Grid.offsets);
	Grid.block = dim3(x, y);
	int threadsperblock = z;
	init_neighbour<<<Grid.block,threadsperblock>>>(Grid.neighbours,Grid.nbcounts);
	CheckError();
	CheckError();
}

__global__ void init_neighbour(unsigned* neighbours,unsigned* nbcounts)
{
	int index = blockIdx.x * gridDim.y * blockDim.x + blockIdx.y * blockDim.x + threadIdx.x;
	int _x = blockIdx.x;
	int _y = blockIdx.y;
	int _z = threadIdx.x;
	unsigned count = 0;
	for (int x = -1; x < 2; ++x)
	{
		for (int y = -1; y < 2; ++y)
		{
			for (int z = -1; z < 2; ++z)
			{
				if (_x + x >= 0 && _x + x < gridDim.x
					&& _y + y >= 0 && _y + y < gridDim.y
					&& _z + z >= 0 && _z + z < blockDim.x)
				{
					neighbours[index*27+count] = (_x + x)*gridDim.y*blockDim.x + (_y + y)*blockDim.x + _z + z;
					count++;
				}
			}
		}
	}
	nbcounts[index] = count;
}

__global__ void clear_grid(unsigned* counts, unsigned* offsets)
{
	int index = blockIdx.x * gridDim.y * blockDim.x + blockIdx.y * blockDim.x + threadIdx.x;
	counts[index] = 0;
	offsets[index] = 0;
}