#pragma once
#include "errorcheck.h"

struct grid
{
	int xdim;
	int ydim;
	int zdim;
	int yzdim;
	int xyzdim;
	dim3 block;
	//neighbours store the neighbourhood cells,26 for 1 cell,27 store counts;
	unsigned* neighbours;
	//neighbour counts
	unsigned* nbcounts;
	//counts track how many particles exist in each cell;
	unsigned* counts;
	//offsets track the starting point in the sorted particle array for each cell  
	unsigned* offsets;
	//Inorder to be overwrite by thrust, the pointer is wraped.
	thrust::device_ptr<unsigned> t_counts;
	thrust::device_ptr<unsigned> t_offsets;
};


void initgrid(int x, int y, int z, grid& Grid);

__global__ void init_neighbour(unsigned* neighbour,unsigned* nbcounts);

__global__ void clear_grid(unsigned* counts, unsigned* offsets);

