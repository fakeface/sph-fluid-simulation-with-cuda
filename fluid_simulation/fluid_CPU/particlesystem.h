#pragma once
#include <graphics_framework.h>
#include <vector>

using namespace std;

struct sphparam
{
	float h;
	float hxh;
	float gamma;
	float K;
	float EK;
	float sigma;
	float rho;
	float zeta;
	float s_threshold;
	float s_co;
	float viscosity_co;
	float max_velocity;
	float max_velocity_sqrt;
	float max_acceleration;
	float max_acceleration_sqrt;
	float damp;
	float kernel_poly6;
	float kernel_poly6_gradient;
	float kernel_poly6_lapalacian;
	float kernel_spiky;
	float kernel_spiky_gradient;
	float kernel_spiky_laplacian;
};

struct wall
{
	glm::vec3 position;
	glm::vec3 normal;
};

struct grid
{
	vector<array<unsigned,28>> neighbour;
	vector<vector<unsigned>> pindex;
};

class particlesystem
{
private:
	//Particle information
	vector<glm::vec3> acceleration;
	vector<glm::vec3> velocity;
	vector<glm::vec3> color;
	vector<float> density;
	vector<float> pressure;
	vector<pair<unsigned,unsigned>> cellindex;
	
	//vector<vector<unsigned>> nbpindex;
	//Sph parameters
	vector<sphparam> parameter;
	unsigned current = 0;
	//Bounding wall
	wall bbox[6];
	//Grid
	grid Grid;
	//Test scenes
	enum test
	{
		Dambreak,
		Droplets,
	};
	//systeminformation
	float container_size;
	float cellsize;
	unsigned gridsize;
	float mass;
	float radius;
	float deltatime;
	float hook_wall = 400.0f;
public:
	particlesystem(){};
	~particlesystem(){};

	vector<glm::vec3> position;
	unsigned number;
	unsigned initial_number;
	bool initialise(unsigned particle_num, float container, float _cellsize, float _mass, float _radius, float _deltatime) ;
	void add(unsigned _num);
	void sorting();
	void updating();
	void integrate();
	void draw();
};
