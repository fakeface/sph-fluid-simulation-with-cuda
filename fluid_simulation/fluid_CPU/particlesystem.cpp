#include "particlesystem.h"

bool particlesystem::initialise(unsigned particle_num, float container, float _cellsize, float _mass, float _radius, float _deltatime)
{
	initial_number = particle_num;
	container_size = container;
	cellsize = _cellsize;
	mass = _mass;
	radius = _radius;
	deltatime = _deltatime;

	//Initialise the particles
	for (unsigned i = 0; i < initial_number; ++i)
	{
		acceleration.push_back(glm::vec3(0.0f));
		velocity.push_back(glm::vec3(0.0f));
		density.push_back(0.0f);
		pressure.push_back(0.0f);
		cellindex.push_back(0);
	}
	float size = powf(initial_number, 1.0f / 3) +1;
	for (unsigned x = 0; x < size; ++x)
	{
		for (unsigned y = 0; y < size; ++y)
		{
			for (unsigned z = 0; z < size; ++z)
			{
				if (position.size() < initial_number)
				position.push_back(glm::vec3(radius + x * 2 * radius, radius + y * 2 * radius, radius + z*2*radius));
			}
		}
	}
	//This stores the neighbour particle id for each particle.
	nbpindex.resize(initial_number);
	color.resize(initial_number);
	/*assert(acceleration.size()>initial_number);
	assert(position.size() > initial_number);*/
	//Initialise Grid
	gridsize = (unsigned)(container_size / cellsize);
	Grid.neighbour.resize(pow(gridsize, 3));
	Grid.pindex.resize(pow(gridsize, 3));
	for (int x = 0; x < gridsize; ++x)
	{
		for (int y = 0; y < gridsize; ++y)
		{
			for (int z = 0; z < gridsize; ++z)
			{
				//For each cell, store the neighbour cells
				unsigned count = 0;
				//Get the current index
				unsigned index = x*gridsize*gridsize + y*gridsize + z;
				//Loop from -1 to 1, 27 cell in total including itself and count the neighbour number
				for (int i = -1; i < 2; ++i)
				{
					for (int j = -1; j < 2; ++j)
					{
						for (int q = -1; q < 2; ++q)
						{
							//Out of range, return
							if (x + i < 0 || x + i>=gridsize ||
								y + j < 0 || y + j>=gridsize ||
								z + q < 0 || z + q>=gridsize)
								continue;
							//Store the neighbour index
							Grid.neighbour[index][count] = (x + i)*gridsize*gridsize + (y + j)*gridsize + z+q;
							count++;
						}
					}
				}
				//The last cell of each cell is storing how many neighbours do they have
				Grid.neighbour[index][27] = count;
				//count may cause one less indexing
			}
		}
	}
	//Initialise the bounding box
	wall yplus;
	yplus.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	yplus.position = glm::vec3(0.0f);
	bbox[0] = yplus;

	wall yminus;
	yminus.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	yminus.position = glm::vec3(0.0f, container_size, 0.0f);
	bbox[1] = yminus;

	wall xplus;
	xplus.normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	xplus.position = glm::vec3(0.0f);
	bbox[2] = xplus;

	wall xminus;
	xminus.normal = glm::vec3(1.0f, 0.0f, 0.0f);
	xminus.position = glm::vec3(container_size, 0.0f, 0.0f);
	bbox[3] = xminus;

	wall zplus;
	zplus.normal = glm::vec3(0.0f, 0.0f, -1.0f);
	zplus.position = glm::vec3(0.0f, 0.0f, 0.0f);
	bbox[4] = zplus;

	wall zminus;
	zminus.normal = glm::vec3(0.0f, 0.0f, 1.0f);
	zminus.position = glm::vec3(0.0f, 0.0f, container_size);
	bbox[5] = zminus;
	//SPH parameters
	float PI = 3.1415926f;
	sphparam dambreak;
	dambreak.h = radius*3;
	dambreak.hxh = dambreak.h*dambreak.h;
	dambreak.K = 7.0f;
	dambreak.rho = 40.0f;
	dambreak.sigma = 0.0f;
	dambreak.zeta = -1.0f;
	dambreak.viscosity_co = 0.008f;
	dambreak.s_threshold = 2.0f;
	dambreak.s_co = 1.0f;
	dambreak.damp = 0.6f;
	dambreak.kernel_poly6 = 315.0f / (64.0f*PI*powf(dambreak.h, 9.0f));
	dambreak.kernel_poly6_gradient = -945.0f / (32.0f*PI*powf(dambreak.h, 9.0f));
	dambreak.kernel_poly6_lapalacian = 945.0f / (8.0f*PI*powf(dambreak.h, 9.0f));
	dambreak.kernel_spiky = 15.0f / (PI*powf(dambreak.h, 6.0f));
	dambreak.kernel_spiky_gradient = -45.0f / (PI*powf(dambreak.h, 6.0f));
	dambreak.kernel_spiky_laplacian = 45.0f / (PI*powf(dambreak.h, 6.0f));
	parameter.push_back(dambreak);

	number = initial_number;
	return true;
}

void particlesystem::sorting()
{
	for (unsigned i = 0; i < number; ++i)
	{
		int x, y, z;
		x = (unsigned)(position[i].x / cellsize);
		if (x < 0) x = 0;
		if (x >= gridsize) x = gridsize - 1;
		y = (unsigned)(position[i].y / cellsize);
		if (y < 0) y = 0;
		if (y >= gridsize) y = gridsize - 1;
		z = (unsigned)(position[i].z / cellsize);
		if (z < 0) z = 0;
		if (z >= gridsize) z = gridsize - 1;
		unsigned index = x*gridsize*gridsize + y*gridsize + z;
		Grid.pindex[index].push_back(i);
		cellindex[i] = index;
	}
}

void particlesystem::updating()
{
	//Loop all the particles
	for (unsigned i = 0; i < number; ++i)
	{
		float dens = 0.0f;
		//Use the cellindex array find out which cell this particle belongs to
		//And query this cell's neighbours to find neighbour particles
		//Grid.neighbour[cellindex[i]][27] store the neighbour counts
		unsigned cellid = cellindex[i];
		for (unsigned j = 0; j < Grid.neighbour[cellid][27]; ++j)
		{
			//The current neighbour cell id
			unsigned nbcellid = Grid.neighbour[cellid][j];
			//Loop all the particles in this cell
			for (unsigned q = 0; q < Grid.pindex[nbcellid].size(); ++q)
			{
				//The particles in this neighbour cell
				unsigned nbpid = Grid.pindex[nbcellid][q];
				float dist = glm::length(position[i] - position[nbpid]);
				//If out of range then jump to next one.
				if (dist > parameter[current].h || nbpid==i) continue;
				//Store the neighbour particle id and the dist
				nbpindex[i].push_back(make_pair(nbpid,dist));
				float check = powf(parameter[current].hxh - dist*dist, 3.0f);
				dens += mass * parameter[current].kernel_poly6 * powf(parameter[current].hxh - dist*dist, 3.0f);
			}
		}
		dens = max(dens, parameter[current].rho);
		density[i] = dens;
		pressure[i] = parameter[current].K * (dens - parameter[current].rho);
	}
	//clear the vector and resize it
	Grid.pindex.clear();
	Grid.pindex.resize(pow(gridsize, 3));
	//Loop all the particles
	for (unsigned i = 0; i < number; ++i)
	{
		glm::vec3 acc = glm::vec3(0.0f);
		for (unsigned q = 0; q < nbpindex[i].size(); ++q)
		{
			unsigned nbid = nbpindex[i][q].first;
			glm::vec3 direction = glm::normalize(position[i] - position[nbid]);
			if (isnan(direction.x))continue;
			float dist = nbpindex[i][q].second;
			acc += -(pressure[i] + pressure[nbid]) / (2 * density[nbid])
				*parameter[current].kernel_spiky_gradient
				*powf(parameter[current].h - dist, 2.0f)
				*direction;
			/*acc += parameter[current].viscosity_co* parameter[current].kernel_spiky_laplacian
				*(parameter[current].h - dist) / density[nbid]
				* (velocity[nbid] - velocity[i]);*/
		}
		acceleration[i] += acc;
	}
	//printf("%d", acceleration.size());
	nbpindex.clear();
	nbpindex.resize(number);
}

void particlesystem::integrate()
{
	for (unsigned i = 0; i < number; ++i)
	{
		//Bounding box collision
		for (unsigned q = 0; q < 6; ++q)
		{
			float penetration = - glm::dot(bbox[q].position - position[i], bbox[q].normal);
			if (penetration <= 0) continue;
			acceleration[i] += -bbox[q].normal*penetration*hook_wall - velocity[i] * 2.0f;
		}
		acceleration[i] += glm::vec3(0.0f, -9.8f, 0.0f);
		velocity[i] += acceleration[i] * deltatime;
		position[i] += velocity[i] * deltatime;
		acceleration[i] = glm::vec3(0.0f);
	}
}

void particlesystem::draw()
{
	float rest = parameter[current].rho;
	float maximum = rest * 3;
	float rate = 0.0f;
	for (unsigned i = 0; i < number; ++i)
	{
		rate = (density[i] - rest) / maximum;
		color[i] = glm::vec3(rate, rate / 2, 1.0f - rate);
	}
}