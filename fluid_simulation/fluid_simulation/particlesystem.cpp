/*
Haoyang Hou.

*/

#include "particlesystem.h"

float radius = 0.2f;
vector<ParticleSystem::Wall> cubesides;
int water_num = 100;
float cubesize = 10.0f;
float cubelength = 10.0f;
float hook_wall = 1000.5f;
float damp = 0.01f;
float hook_ball = 100.5f;
int gridsize = cubelength / (radius * 2);
int mc_level = 100;
//int cells = gridsize * gridsize * gridsize;
int totalcells = gridsize * gridsize * gridsize;
//int totalcells = (gridsize + 1) * (gridsize + 1) * (gridsize + 1);
//timer_chrono timer1(30,"optimized_"+to_string(water_num),"o_"+to_string(water_num),false);
ParticleSystem::ParticleSystem()
{
	//cloth = new Cloth(2, 4, 20, 80);
	water = new Water(water_num, glm::vec3(0.0f), glm::vec3(10.0f));
}

int ParticleSystem::InitParticles(glm::vec3 _centre)
{
	centre = _centre;
	water->initialise();
	//cloth->Initialise();
	Wall yplus;
	yplus.normal = glm::vec3(0.0f, -1.0f, 0.0f);
	yplus.wall = glm::vec3(0.0f);
	cubesides.push_back(yplus);

	Wall yminus;
	yminus.normal = glm::vec3(0.0f, 1.0f, 0.0f);
	yminus.wall = glm::vec3(0.0f, cubesize, 0.0f);
	cubesides.push_back(yminus);

	Wall xplus;
	xplus.normal = glm::vec3(-1.0f, 0.0f, 0.0f);
	xplus.wall = glm::vec3(0.0f);
	cubesides.push_back(xplus);

	Wall xminus;
	xminus.normal = glm::vec3(1.0f, 0.0f, 0.0f);
	xminus.wall = glm::vec3(cubesize, 0.0f, 0.0f);
	cubesides.push_back(xminus);

	Wall zplus;
	zplus.normal = glm::vec3(0.0f, 0.0f, -1.0f);
	zplus.wall = glm::vec3(0.0f, 0.0f, 0.0f);
	cubesides.push_back(zplus);

	Wall zminus;
	zminus.normal = glm::vec3(0.0f, 0.0f, 1.0f);
	zminus.wall = glm::vec3(0.0f, 0.0f, cubesize);
	cubesides.push_back(zminus);

	//partition.resize(u_cells);
	//partition.reserve(gridsize*gridsize*gridsize);
	partition.resize(totalcells);
	
	return 0;
}

void ParticleSystem::Draw()
{
	water->draw(radius);
	//cloth->Draw();
}

void ParticleSystem::AddForce(const glm::vec3 direction)
{
	water->addForce(direction);
	//cloth->AddForce(direction*0.1);
	//cloth->WindForce(glm::vec3(0.5f, 0.1f, 0.0f));
}

void ParticleSystem::Intergrate(float deltaTime)
{
	water->integrate(deltaTime);
	//cloth->Integrate(deltaTime);
}

void ParticleSystem::Sorting()
{
	for (int i = 0; i < water->particles.size(); i++)
	{
		int x = 0, y = 0, z = 0;
		glm::vec3 pos = water->particles[i].getPos();
		if (pos.x < 0 || pos.x > 10 || pos.y > 10 || pos.y < 0 || pos.z > 10 || pos.z < 0) continue;

		x = (int)((pos.x ) / (radius * 2));
		y = (int)((pos.y ) / (radius * 2));
		z = (int)((pos.z ) / (radius * 2));

		int total = x * gridsize * gridsize + y * gridsize + z;
		partition[total].push_back(&water->particles[i]);
	}
}

void ParticleSystem::CollisionDetection(float deltaTime)
{
	//timer1.start_timing();

	Sorting();

	/*for (int i = 0; i < water->particles.size()-1; ++i)
	{
	for (int q = i + 1; q < water->particles.size(); q++)
	{
	CollisionHandler(&water->particles[i], &water->particles[q]);
	}
	}*/
	//for (int i = 0; i < partition.size(); ++i)
	//{
	//	if (partition[i].size() != 0)
	//	{
	//		for (int m = 0; m < partition[i].size(); m++)
	//		{
	//			for (int n = m + 1; n < partition[i].size() - 1; n++)
	//			{
	//				CollisionHandler(partition[i][m], partition[i][n]);
	//			}
	//		}
	//		for (int x = -1; x <= 1; ++x)
	//		{
	//			for (int y = -1; y <= 1; ++y)
	//			{
	//				for (int z = -1; z <= 1; ++z)
	//				{
	//					CollisionHandler(&partition[i], &partition[i + x*gridsize*gridsize + y*gridsize + z]);
	//				}
	//			}
	//		}
	//	}
	//}
	//for (int x = -1; x <= 1; ++x)
	//{
	//	for (int y = -1; y <= 1; ++y)
	//	{
	//		for (int z = -1; z <= 1; ++z)
	//		{

	//		}
	//	}
	//}
	for (int i = 0; i < gridsize; ++i)
	{
		for (int j = 0; j < gridsize; ++j)
		{
			for (int q = 0; q < gridsize; ++q)
			{
				int cell_origin = i * gridsize * gridsize + j * gridsize + q;
				if (partition[cell_origin].size() != 0)
				{
					for (int m = 0; m < partition[cell_origin].size(); m++)
					{
						for (int n = m + 1; n < partition[cell_origin].size() - 1; n++)
						{
							CollisionHandler(partition[cell_origin][m], partition[cell_origin][n]);
						}
					}
					if (i*j*q >0 && i < gridsize - 1 && j < gridsize - 1 && q < gridsize - 1)
					{
						for (int x = -1; x <= 1; ++x)
						{
							for (int y = -1; y <= 1; ++y)
							{
								for (int z = -1; z <= 1; ++z)
								{
									if (x == 0 && y == 0 && z == 0) continue;
									CollisionHandler(&partition[cell_origin], &partition[cell_origin + x * gridsize * gridsize + y * gridsize + z]);
								}
							}
						}
					}
				}
			}
		}

	}

	for (int i = 0; i < totalcells; ++i)
	{
		partition[i].clear();
	}

	std::vector<Particle>::iterator particle;

	for (particle = water->particles.begin(); particle != water->particles.end(); particle++)
	{
		//get the velocity of this particle.
		glm::vec3 velocity = (*particle).getVelocity();
		glm::vec3 pos = (*particle).getPos();
		for (auto i : cubesides)
		{
			float penalty = radius - glm::dot(i.wall - pos, i.normal);
			if (penalty <= 0) continue;
			particle->addForce(-i.normal*penalty*hook_wall);
			particle->addForce(-velocity * 1.0f);
		}
		//cloth->BallCollision(pos, radius);
	}
	//timer1.stop_timing();
}

void ParticleSystem::CollisionHandler(Particle *p1, Particle *p2)
{
	if (p1 == NULL || p2 == NULL) return;
	glm::vec3 dis = (*p1).getPos() - (*p2).getPos();		//get the current distance.
	float length = glm::length(dis);							//get length of distance.
	glm::vec3 normal = glm::normalize(dis);

	float penalty = 2 * radius - length;
	if (penalty > 0)
	{
		(*p1).addForce(normal*penalty*hook_ball);
		(*p2).addForce(-normal*penalty*hook_ball);
	}

}

void ParticleSystem::CollisionHandler(vector<Particle*> *cell_1, vector<Particle*> *cell_2)
{
	if (cell_1->size() != 0 && cell_2->size() != 0)
	{
		for (int i = 0; i < cell_1->size(); ++i)
		{
			for (int j = 0; j < cell_2->size(); ++j)
				CollisionHandler((*cell_1)[i], (*cell_2)[j]);
		}
	}
}

