#include "water.h"

// Forward declaration of a pointer
class Water;
class ParticleSystem{
private:
	glm::vec3 centre;
public:
	Water* water;
	vector<vector<Particle*>> partition;
	struct Wall
	{
		glm::vec3 wall;
		glm::vec3 normal;
	};
	ParticleSystem();
	virtual int InitParticles(glm::vec3 _centre);
	virtual void Draw();
	virtual void Intergrate(float deltaTime);
	virtual void AddForce(const glm::vec3 direction);
	virtual void CollisionHandler(Particle *p1, Particle *p2);
	virtual void CollisionHandler(vector<Particle*> * cell_1, vector<Particle*> * cell_2);
	virtual void CollisionDetection(float deltaTime);
	virtual void Sorting();
};