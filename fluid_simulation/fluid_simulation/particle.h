#pragma once
#include <graphics_framework.h>
#include <iostream>

using namespace std;
using namespace graphics_framework;

class Particle
{
private:
	float inverseMass;
	float dampValue;
	float viscosity;
	glm::vec3 force;
	glm::vec3 pos;
	glm::vec3 oldPos;
	glm::vec3 acceleration;
	glm::vec3 velocity;
public:
	Particle(const glm::vec3& pos) : pos(pos), oldPos(pos), acceleration(glm::vec3(0.0f, 0.0f, 0.0f)), velocity(glm::vec3(0.0f, 0.0f, 0.0f))
		, force(glm::vec3(0.0f, 0.0f, 0.0f)), inverseMass(100.0f), dampValue(0.9f){}
	Particle(){}

	void addForce(glm::vec3 f)
	{
		acceleration += f * inverseMass;
		//cout<<"xx"<<accerlation.y
	}

	void integrate(float deltaTime)
	{
		velocity += acceleration * deltaTime * dampValue;
		pos += velocity * deltaTime;
		//DBG_ASSERT(position.x >= 0.0f);
		acceleration = glm::vec3(0, 0, 0);
		force = glm::vec3(0, 0, 0);
	}
	void setpos(const glm::vec3& v) { pos = v; }
	void setVelocity(const glm::vec3& v) { velocity = v; }
	const glm::vec3& getPos() const { return pos; }
	const glm::vec3& getVelocity() const { return velocity; }
	void offsetPos(const glm::vec3& v) { pos += v; }
};