#include "water.h"

Water::Water(int num, glm::vec3 _min, glm::vec3 _max) :num_particles(num),minrange(_min), maxrange(_max){}


void Water::initialise()
{
	//particle position generator
	default_random_engine s;
	uniform_real_distribution<float> x(minrange.x, maxrange.x);
	uniform_real_distribution<float> y(minrange.y, maxrange.y);
	uniform_real_distribution<float> z(minrange.z, maxrange.z);
	for (int i = 0; i < num_particles; i++){
		particles.push_back(Particle(glm::vec3(x(s), y(s), z(s))));
		particles.back().addForce(glm::vec3(2.5f, -2.5f, 0.0f));
		water_mesh.push_back(geometry_builder::create_sphere());
	}

}

void Water::draw(float radius)
{

	//static GLuint mysphere = 0;
	//if (mysphere == 0)
	//{
	//	mysphere = glGenLists(1);
	//	glNewList(mysphere, GL_COMPILE);
	//	glutSolidSphere(radius, 10, 10);
	//	glEndList();
	//}
	////std::vector<Particle>::iterator particle;
	//for (particle = particles.begin(); particle != particles.end(); particle++)
	//{
	//	glColor3f(0.0, 0.7, 1.0);
	//	//glPushMatrix();

	//	//Get the current position and draw.
	//	const glm::vec3 pos = (*particle).getPos();
	//	glTranslatef(pos.x, pos.y, pos.z);
	//	//glutSolidSphere(0.5, 15, 15); 

	//	glCallList(mysphere);

	//	glTranslatef(-pos.x, -pos.y, -pos.z);
	//}
	////particle = 
}

void Water::addForce(const glm::vec3 direction)
{
	//std::vector<Particle>::iterator particle;
	for (particle = particles.begin(); particle != particles.end(); particle++)
	{
		(*particle).addForce(direction);
	}
}

void Water::integrate(float deltaTime)
{
	for (particle = particles.begin(); particle != particles.end(); particle++)
	{
		(*particle).integrate(deltaTime);
	}
}

